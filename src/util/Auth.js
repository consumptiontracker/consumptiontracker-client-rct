const AUTH_TOKEN = 'CTREACT_AUTH_TOKEN'
const AUTH_USERNAME = 'CTREACT_AUTH_USERNAME'

function clearAuthentication() {
  localStorage.removeItem(AUTH_TOKEN)
  localStorage.removeItem(AUTH_USERNAME)
}

function storeAuthentication(session) {
  localStorage.setItem(AUTH_TOKEN, session.token)
  localStorage.setItem(AUTH_USERNAME, session.username)
}

function getUsername() {
  return localStorage.getItem(AUTH_USERNAME)
}

function getToken() {
  return localStorage.getItem(AUTH_TOKEN)
}

const Auth = {
  clearAuthentication,
  storeAuthentication,
  getUsername,
  getToken,
}

export default Auth
