import Auth from './Auth'

// Constants
export const ERROR_UNAUTHORIZED = 'Unauthorized'
export const ERROR_FAILED_TO_FETCH = 'Failed to fetch'

// Response and error handlers

function handleResponse(response) {
  if (response.status >= 300 && response.status <= 400) {
    throw new Error(`Fetch request failed with status ${response.status}`)
  } else if (response.status >= 401 && response.status < 500) {
    Auth.clearAuthentication()
    throw new Error(ERROR_UNAUTHORIZED)
  }
}

async function handleJsonResponse(response) {
  if (response.status >= 300 && response.status <= 400) {
    throw new Error(`Fetch request failed with status ${response.status}`)
  } else if (response.status >= 401 && response.status < 500) {
    Auth.clearAuthentication()
    throw new Error(ERROR_UNAUTHORIZED)
  }
  return await response.json()
}

function handleRoutingOnError(e, navigate) {
  console.log('ERROR', e)
  if (e.message === ERROR_UNAUTHORIZED) {
    navigate('/login')
  } else {
    navigate('/error')
  }
}

// Consumption processing functions

function getMeasurement(recordingDate, gauge, allMeasurements) {
  if (!recordingDate || !gauge || !allMeasurements) {
    return null
  }
  return allMeasurements
    .filter((measurement) => measurement.gaugeId === gauge.id)
    .find((measurement) => measurement.recordingDate === recordingDate)
}

function getPreviousMeasurement(currentMeasurement, allMeasurements) {
  return getPreviousMeasurementForRecordingDate(
    currentMeasurement.recordingDate,
    currentMeasurement.gauge,
    allMeasurements
  )
}

function getPreviousMeasurementForRecordingDate(
  recordingDate,
  gauge,
  allMeasurements
) {
  return allMeasurements?.find(
    (measurement) =>
      measurement.recordingDate < recordingDate &&
      measurement.gauge.type === gauge.type &&
      measurement.gauge.name === gauge.name
  )
}

function getConsumption(measurement, previousMeasurement) {
  const currentMeasurementDelta = measurement.value - measurement.offset
  const previousMeasurementDelta = previousMeasurement
    ? previousMeasurement.value - previousMeasurement.offset
    : 0
  return currentMeasurementDelta - previousMeasurementDelta
}

// Math functions

function round(value) {
  return value ? Math.round(value * 100) / 100 : value
}

const Util = {
  handleResponse,
  handleJsonResponse,
  handleRoutingOnError,
  getMeasurement,
  getPreviousMeasurement,
  getPreviousMeasurementForRecordingDate,
  getConsumption,
  round,
}

export default Util
