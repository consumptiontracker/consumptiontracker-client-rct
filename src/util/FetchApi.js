import Util from './Util'
import Auth from './Auth'

const login = async (credentials) => {
  const response = await fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(credentials),
  })
  return await Util.handleJsonResponse(response)
}

const getGauges = async () => {
  const response = await fetch(`${process.env.REACT_APP_API_URL}/gauges`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${Auth.getToken()}`,
    },
  })
  return await Util.handleJsonResponse(response)
}

const getMeasurements = async () => {
  const response = await fetch(
    `${process.env.REACT_APP_API_URL}/measurements`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${Auth.getToken()}`,
      },
    }
  )
  return await Util.handleJsonResponse(response)
}

const postMeasurements = async (measurements) => {
  await fetch(`${process.env.REACT_APP_API_URL}/measurements`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${Auth.getToken()}`,
      'Content-type': 'application/json',
    },
    body: JSON.stringify(measurements),
  })
}

const FetchApi = {
  login,
  getGauges,
  getMeasurements,
  postMeasurements,
}

export default FetchApi
