import { useDispatch } from 'react-redux'
import { UserActions } from '../store/UserSlice'

const CtError = ({ message }) => {
  const dispatch = useDispatch()

  const closeErrorBox = () => dispatch(UserActions.setError(null))

  return (
    message && (
      <div className="ct-error-box" onClick={closeErrorBox}>
        {message}
      </div>
    )
  )
}

export default CtError
