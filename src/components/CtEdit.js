import { useState, useCallback, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import ReportingCalendar from '../util/ReportingCalendar'
import Util from '../util/Util'
import CtMeasurementInput from './CtMeasurementInput'
import { postMeasurements } from '../store/ConsumptionActions'
import CtMessage from './CtMessage'
import { ConsumptionActions } from '../store/ConsumptionSlice'

const CtEdit = () => {
  const dispatch = useDispatch()

  const [reportingDateStr, setReportingDateStr] = useState(
    ReportingCalendar.currentMonthReportingDate()
  )

  const savedAt = useSelector((state) => state.consumption.savedAt)
  const gauges = useSelector((state) => state.consumption.gauges)
  const allMeasurements = useSelector((state) => state.consumption.measurements)

  const [selectedMeasurements, setSelectedMeasurements] = useState([])

  const back = () => {
    setReportingDateStr(
      ReportingCalendar.previousReportingDate(reportingDateStr),
      () =>
        setSelectedMeasurements(
          getMeasurementsForReportingDate(
            reportingDateStr,
            gauges,
            allMeasurements
          )
        )
    )
  }

  const forward = () => {
    setReportingDateStr(
      ReportingCalendar.nextReportingDate(reportingDateStr),
      () =>
        setSelectedMeasurements(
          getMeasurementsForReportingDate(
            reportingDateStr,
            gauges,
            allMeasurements
          )
        )
    )
  }

  const handleMeasurementValueChange = (name, type, value) =>
    setSelectedMeasurements((currentSelectedMeasurements) => {
      currentSelectedMeasurements.find(
        (measurement) =>
          measurement.gauge.name === name && measurement.gauge.type === type
      ).value = value
      return [...currentSelectedMeasurements]
    })

  const handleMeasurementOffsetChange = (name, type, offset) =>
    setSelectedMeasurements((currentSelectedMeasurements) => {
      currentSelectedMeasurements.find(
        (measurement) =>
          measurement.gauge.name === name && measurement.gauge.type === type
      ).offset = offset
      return [...currentSelectedMeasurements]
    })

  const handleSubmit = async (e) => {
    e.preventDefault()
    const measurementsToSave = selectedMeasurements.filter(
      (measurement) => measurement.value
    )
    if (measurementsToSave.length > 0) {
      dispatch(postMeasurements(measurementsToSave))
    }
  }

  const setupSelectedMeasurements = useCallback(async () => {
    const calculatedSelectedMeasurements = getMeasurementsForReportingDate(
      reportingDateStr,
      gauges,
      allMeasurements
    )
    setSelectedMeasurements(calculatedSelectedMeasurements)
  }, [reportingDateStr, gauges, allMeasurements])

  useEffect(() => {
    setupSelectedMeasurements()
  }, [setupSelectedMeasurements])

  return (
    <form onSubmit={handleSubmit}>
      {
        <CtMessage
          message="Success"
          triggerDisplay={savedAt}
          displayFinished={() => dispatch(ConsumptionActions.clearSavedAt())}
        />
      }
      <div className="ct-button-bar-container">
        <div className="ct-button-bar">
          <div className="ct-nav-button ct-nav-button-previous" onClick={back}>
            &lt;
          </div>
          <div className="ct-month-label">{reportingDateStr}</div>
          <div className="ct-nav-button ct-nav-button-next" onClick={forward}>
            &gt;
          </div>
        </div>
      </div>
      <div className="ct-measurement-input-container">
        {selectedMeasurements &&
          selectedMeasurements.map((measurement) => (
            <CtMeasurementInput
              key={measurement.gauge.id}
              measurement={measurement}
              handleMeasurementValueChange={handleMeasurementValueChange}
              handleMeasurementOffsetChange={handleMeasurementOffsetChange}
            />
          ))}
      </div>
      <div className="ct-op-button-container">
        <button className="ct-button ct-text-light-2-strong ct-center-content">
          Save
        </button>
      </div>
    </form>
  )
}

function getMeasurementsForReportingDate(
  reportingDate,
  gauges,
  allMeasurements
) {
  return gauges.map((gauge) => {
    let measurement = {
      ...Util.getMeasurement(reportingDate, gauge, allMeasurements),
    }
    if (!measurement.id) {
      const previousMeasurement = Util.getPreviousMeasurementForRecordingDate(
        reportingDate,
        gauge,
        allMeasurements
      )
      measurement = {
        gaugeId: gauge.id,
        gauge,
        recordingDate: reportingDate,
        value: '',
        offset: previousMeasurement?.offset || '',
      }
    }
    return measurement
  })
}

export default CtEdit
