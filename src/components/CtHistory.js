import CtHistoryItem from './CtHistoryItem'
import { useSelector } from 'react-redux'

const History = () => {
  const stats = useSelector((state) => state.consumption.stats)

  return (
    <div>
      {stats &&
        stats.periodStatsItems.map((periodStatsItem) => (
          <CtHistoryItem
            key={periodStatsItem.recordingDate}
            periodStatsItem={periodStatsItem}
          />
        ))}
    </div>
  )
}

export default History
