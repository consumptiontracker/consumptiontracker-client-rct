import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { ConsumptionActions } from '../store/ConsumptionSlice'
import { UserActions } from '../store/UserSlice'

const CtHeader = () => {
  const dispatch = useDispatch()
  const username = useSelector((state) => state.user.username)
  const [navOpen, setNavOpen] = useState('false')
  const toggleNav = () =>
    setNavOpen((current) => (current === 'false' ? 'true' : 'false'))
  const navigate = useNavigate()
  const navigateTo = (target) => {
    navigate(target)
    toggleNav()
  }
  const logout = () => {
    dispatch(UserActions.logout())
    dispatch(ConsumptionActions.clearConsumptionData())
    navigateTo('/login')
  }

  return (
    <div>
      {username && (
        <header className="ct-header">
          <div className="ct-button-menu" onClick={toggleNav}>
            <div className="ct-stripe"></div>
            <div className="ct-stripe"></div>
            <div className="ct-stripe"></div>
          </div>
          <div className="ct-user">{username}</div>
        </header>
      )}
      <nav className={`ct-nav ${navOpen === 'true' && 'ct-nav-open'}`}>
        <div className="ct-nav-top-bar">
          <div>
            <button className="ct-button" onClick={toggleNav}>
              Close
            </button>
          </div>
        </div>
        <div className="ct-nav-item" onClick={() => navigateTo('/')}>
          Add and edit
        </div>
        <div className="ct-nav-item" onClick={() => navigateTo('/history')}>
          History
        </div>
        <div className="ct-nav-item" onClick={() => navigateTo('/gauges')}>
          Gauges
        </div>
        <div className="ct-nav-item" onClick={() => navigateTo('/about')}>
          About
        </div>
        <div className="ct-nav-log-out">
          <div>
            <button className="ct-button" onClick={logout}>
              Log out
            </button>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default CtHeader
