const UnitParser = ({ from }) =>
  from === 'm3' ? (
    <span>
      m<sup>3</sup>
    </span>
  ) : (
    <span>{from}</span>
  )

export default UnitParser
