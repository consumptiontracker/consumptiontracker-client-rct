import { useState } from 'react'
import UnitParser from './UnitParser'
import Util from '../util/Util'

const CtHistoryItem = ({ periodStatsItem }) => {
  const [cardOpen, setCardOpen] = useState('false')
  const toggleCard = () => setCardOpen(cardOpen === 'false' ? 'true' : 'false')

  return (
    <div className="ct-card">
      <div className="ct-card-header ct-clickable-area" onClick={toggleCard}>
        {periodStatsItem.recordingDate}
      </div>
      {cardOpen === 'true' &&
        periodStatsItem.gaugeTypeStatsItems.map((gaugeTypeStatsItem) => (
          <div className="ct-card-row" key={gaugeTypeStatsItem.type}>
            <h2 className="ct-text-dark-strong">{gaugeTypeStatsItem.type}</h2>
            <div className="ct-data-row">
              <div className="ct-data-cell-left ct-text-dark-strong">Gauge</div>
              <div className="ct-data-cell-right ct-text-dark-strong">
                Value
              </div>
              <div className="ct-data-cell-right ct-text-dark-strong">
                Cons.
              </div>
            </div>
            {gaugeTypeStatsItem.measurementStatsItems.map(
              (measurementStatsItem) => (
                <div
                  className="ct-data-row"
                  key={measurementStatsItem.gaugeName}
                >
                  <div className="ct-data-cell-left ct-text-dark">
                    {measurementStatsItem.gaugeName}
                  </div>
                  <div className="ct-data-cell-right ct-text-dark">
                    <div>{Util.round(measurementStatsItem.value)}</div>
                    <UnitParser from={gaugeTypeStatsItem.unit} />
                  </div>
                  <div className="ct-data-cell-right ct-text-dark">
                    <div>{Util.round(measurementStatsItem.consumption)}</div>
                    <UnitParser from={gaugeTypeStatsItem.unit} />
                  </div>
                </div>
              )
            )}
            <hr />
            <div className="ct-data-row">
              <div className="ct-data-cell-left ct-text-dark">Total</div>
              <div className="ct-data-cell-right ct-text-dark-strong">
                <div>{Util.round(gaugeTypeStatsItem.consumption)}</div>
                <UnitParser from={gaugeTypeStatsItem.unit} />
              </div>
            </div>
          </div>
        ))}
    </div>
  )
}

export default CtHistoryItem
