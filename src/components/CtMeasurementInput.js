import UnitParser from './UnitParser'

const CtMeasurementInput = ({
  measurement,
  handleMeasurementValueChange,
  handleMeasurementOffsetChange,
}) => {
  return (
    <div className="ct-card">
      <div className="ct-card-header">{measurement.gauge.type}</div>
      <div className="ct-card-row">
        <span className="ct-text-dark-strong">Gauge: </span>
        <span className="ct-text-dark">{measurement.gauge.name}</span>
      </div>
      <div className="ct-card-row ct-text-dark">
        What's the current value at the gauge's display?
      </div>
      <div className="ct-card-row">
        <div className="ct-input-container">
          <div className="ct-label-left ct-label-left-dark ct-text-dark ct-center-content">
            Value
          </div>
          <input
            type="number"
            className="ct-input-center ct-input-center-dark ct-text-dark ct-input-text-center"
            value={measurement.value}
            onChange={(e) =>
              handleMeasurementValueChange(
                measurement.gauge.name,
                measurement.gauge.type,
                e.target.value
              )
            }
          />
          <div className="ct-label-right ct-label-right-dark ct-text-dark ct-center-content">
            <UnitParser from={measurement.gauge.unit} />
          </div>
        </div>
      </div>
      <div className="ct-card-row">
        <div className="ct-input-container">
          <div
            className="ct-label-left ct-label-left-light ct-text-light-1 ct-center-content"
            title="Initial value"
          >
            Offset
          </div>
          <input
            type="number"
            className="ct-input-center ct-input-center-light ct-text-light-1 ct-input-text-center"
            value={measurement.offset}
            onChange={(e) =>
              handleMeasurementOffsetChange(
                measurement.gauge.name,
                measurement.gauge.type,
                e.target.value
              )
            }
          />
          <div className="ct-label-right ct-label-right-light ct-text-light-1 ct-center-content">
            <UnitParser from={measurement.gauge.unit} />
          </div>
        </div>
      </div>
    </div>
  )
}

export default CtMeasurementInput
