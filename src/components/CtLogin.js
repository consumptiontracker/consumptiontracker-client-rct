import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { login } from '../store/UserActions'
import { UserActions } from '../store/UserSlice'

const CtLogin = () => {
  const loginUsername = useSelector((state) => state.user.username)

  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const navigate = useNavigate()
  const dispatch = useDispatch()

  const handleSubmit = async (e) => {
    e.preventDefault()
    if (username.trim().length < 1 || password.trim().length < 1) {
      dispatch(UserActions.setError('Username and password are both needed!'))
      return
    }
    dispatch(UserActions.setError(null))
    dispatch(
      login({
        username,
        password,
      })
    )
  }

  useEffect(() => {
    if (loginUsername) {
      navigate('/')
    }
  }, [loginUsername, navigate])

  return (
    <form className="ct-login" onSubmit={handleSubmit}>
      <div>
        Username:
        <input
          type="text"
          className="ct-input"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
      </div>
      <div>
        Password:
        <input
          type="password"
          className="ct-input"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <div className="ct-login-button-row">
        <button className="ct-button">Login</button>
      </div>
    </form>
  )
}

export default CtLogin
