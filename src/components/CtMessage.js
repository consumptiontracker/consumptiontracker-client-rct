import { useState, useEffect } from 'react'

const CtMessage = ({ message, triggerDisplay, displayFinished }) => {
  const [show, setShow] = useState('false')
  useEffect(() => {
    if (triggerDisplay) {
      setShow('true')
      setTimeout(() => {
        setShow('false')
        displayFinished()
      }, 1000)
    }
  }, [triggerDisplay, displayFinished])

  return (
    message &&
    show === 'true' && <div className="ct-message-box">{message}</div>
  )
}

export default CtMessage
