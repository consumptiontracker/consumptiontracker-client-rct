import UnitParser from './UnitParser'
import { useSelector } from 'react-redux'

const CtGauges = () => {
  const gauges = useSelector((state) => state.consumption.gauges)

  return (
    <div>
      {gauges &&
        gauges.map((gauge) => {
          return (
            <div key={gauge.id} className="ct-card">
              <div className="ct-card-header">{gauge.type}</div>
              <div className="ct-card-row">
                <span className="ct-text-dark-strong">Name: </span>
                <span className="ct-text-dark">{gauge.name}</span>
              </div>
              <div className="ct-card-row">
                <span className="ct-text-dark-strong">Unit: </span>
                <span className="ct-text-dark">
                  <UnitParser from={gauge.unit} />
                </span>
              </div>
              <div className="ct-card-row">
                <span className="ct-text-dark-strong">Active: </span>
                <span className="ct-text-dark">{'' + gauge.active}</span>
              </div>
            </div>
          )
        })}
    </div>
  )
}

export default CtGauges
