export default class StatsContainer {
  periodStatsItems = []

  constructor(measurements) {
    if (measurements.length > 0) {
      let periodStatsMap = new Map()
      measurements.forEach((measurement) => {
        if (!periodStatsMap.get(measurement.recordingDate)) {
          periodStatsMap.set(measurement.recordingDate, [])
        }
        periodStatsMap.get(measurement.recordingDate).push(measurement)
      })
      Array.from(periodStatsMap.values()).forEach((periodMeasurements) =>
        this.periodStatsItems.push(new PeriodStatsEntity(periodMeasurements))
      )
    }
  }
}

class PeriodStatsEntity {
  recordingDate
  gaugeTypeStatsItems = []

  constructor(periodMeasurements) {
    this.recordingDate = periodMeasurements[0].recordingDate

    let gaugeTypeStatsMap = new Map()
    periodMeasurements.forEach((periodMeasurement) => {
      if (!gaugeTypeStatsMap.get(periodMeasurement.gauge.type)) {
        gaugeTypeStatsMap.set(periodMeasurement.gauge.type, [])
      }
      gaugeTypeStatsMap
        .get(periodMeasurement.gauge.type)
        .push(periodMeasurement)
    })
    Array.from(gaugeTypeStatsMap.values()).forEach((periodMeasurements) =>
      this.gaugeTypeStatsItems.push(
        new GaugeTypeStatsEntity(periodMeasurements)
      )
    )
  }
}

class GaugeTypeStatsEntity {
  type
  measurementStatsItems = []
  consumption
  unit

  constructor(measurements) {
    this.type = measurements[0].gauge.type
    this.unit = measurements[0].gauge.unit
    measurements.forEach((measurement) =>
      this.measurementStatsItems.push(new MeasurementStatsEntity(measurement))
    )
    this.measurementStatsItems.sort((m1, m2) => m1.gaugeOrder - m2.gaugeOrder)
    this.consumption = this.measurementStatsItems
      .map((measurement) => measurement.consumption)
      .reduce((p, c) => p + c)
  }
}

class MeasurementStatsEntity {
  gaugeName
  gaugeOrder
  value
  consumption

  constructor(measurement) {
    this.gaugeName = measurement.gauge.name
    this.gaugeOrder = measurement.gauge.order
    this.value = measurement.value
    this.consumption = measurement.consumption
  }
}
