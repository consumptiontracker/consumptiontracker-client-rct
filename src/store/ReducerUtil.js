import { ERROR_FAILED_TO_FETCH, ERROR_UNAUTHORIZED } from '../util/Util'
import Auth from '../util/Auth'

export function handleRejection(state, action, isLogin) {
  if (action.error.message === ERROR_UNAUTHORIZED) {
    Auth.clearAuthentication()
    state.username = null
    state.error = isLogin ? 'Invalid credentials!' : 'Session expired!'
  } else if (action.error.message === ERROR_FAILED_TO_FETCH) {
    state.error = 'Connection failure!'
  } else {
    state.error = `Error: ${action.error.message}`
  }
}
