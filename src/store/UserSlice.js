import { createSlice } from '@reduxjs/toolkit'
import { login } from './UserActions'
import { fetchConsumptionData, postMeasurements } from './ConsumptionActions'
import Auth from '../util/Auth'
import { handleRejection } from './ReducerUtil'

const UserSlice = createSlice({
  name: 'user',
  initialState: {
    username: null,
    error: null,
  },
  reducers: {
    init(state) {
      state.username = Auth.getUsername()
    },
    logout(state) {
      Auth.clearAuthentication()
      state.username = null
    },
    setError(state, action) {
      state.error = action.payload
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(login.fulfilled, (state, action) => {
        state.username = action.payload.user.username
        state.error = null
      })
      .addCase(login.rejected, (state, action) => {
        handleRejection(state, action, true)
      })
      .addCase(fetchConsumptionData.rejected, (state, action) => {
        handleRejection(state, action)
      })
      .addCase(fetchConsumptionData.fulfilled, (state) => {
        state.error = null
      })
      .addCase(postMeasurements.rejected, (state, action) => {
        handleRejection(state, action)
      })
      .addCase(postMeasurements.fulfilled, (state) => {
        state.error = null
      })
  },
})

export const UserActions = UserSlice.actions
export default UserSlice
