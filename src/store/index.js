import { configureStore } from '@reduxjs/toolkit'
import ConsumptionSlice from './ConsumptionSlice'
import UserSlice from './UserSlice'

const store = configureStore({
  reducer: { consumption: ConsumptionSlice.reducer, user: UserSlice.reducer },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }),
})

export default store
