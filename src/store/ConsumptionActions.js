import { createAsyncThunk } from '@reduxjs/toolkit'
import FetchApi from '../util/FetchApi'

export const fetchConsumptionData = createAsyncThunk(
  'consumption/fetchData',
  async () => {
    const [gauges, measurements] = await Promise.all([
      FetchApi.getGauges(),
      FetchApi.getMeasurements(),
    ])
    return { gauges, measurements }
  }
)

export const postMeasurements = createAsyncThunk(
  'consumption/postMeasurements',
  async (measurementsToPost) => {
    await FetchApi.postMeasurements(measurementsToPost)
    const [gauges, measurements] = await Promise.all([
      FetchApi.getGauges(),
      FetchApi.getMeasurements(),
    ])
    return { gauges, measurements }
  }
)
