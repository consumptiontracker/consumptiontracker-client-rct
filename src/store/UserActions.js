import { createAsyncThunk } from '@reduxjs/toolkit'
import Auth from '../util/Auth'
import FetchApi from '../util/FetchApi'

export const login = createAsyncThunk('user/login', async (credentials) => {
  const session = await FetchApi.login(credentials)
  Auth.storeAuthentication(session)
  const [gauges, measurements] = await Promise.all([
    FetchApi.getGauges(),
    FetchApi.getMeasurements(),
  ])
  return {
    gauges,
    measurements,
    user: session,
  }
})
