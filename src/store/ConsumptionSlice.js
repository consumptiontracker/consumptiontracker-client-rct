import { createSlice } from '@reduxjs/toolkit'
import StatsContainer from '../model/StatsContainer'
import { fetchConsumptionData, postMeasurements } from './ConsumptionActions'
import Util from '../util/Util'
import { login } from './UserActions'

const ConsumptionSlice = createSlice({
  name: 'consumption',
  initialState: {
    gauges: [],
    measurements: [],
    savedAt: null,
  },
  reducers: {
    clearConsumptionData(state) {
      state.gauges = []
      state.measurements = []
      state.stats = []
    },
    clearSavedAt(state) {
      state.savedAt = null
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchConsumptionData.fulfilled, (state, action) => {
        const gauges = action.payload.gauges
        const measurements = action.payload.measurements
        adjustMeasurements(gauges, measurements)
        const stats = new StatsContainer(measurements)
        state.gauges = gauges
        state.measurements = measurements
        state.stats = stats
      })
      .addCase(postMeasurements.fulfilled, (state, action) => {
        const gauges = action.payload.gauges
        const measurements = action.payload.measurements
        adjustMeasurements(gauges, measurements)
        const stats = new StatsContainer(measurements)
        state.gauges = gauges
        state.measurements = measurements
        state.stats = stats
        state.savedAt = new Date().toISOString()
      })
      .addCase(login.fulfilled, (state, action) => {
        const gauges = action.payload.gauges
        const measurements = action.payload.measurements
        adjustMeasurements(gauges, measurements)
        const stats = new StatsContainer(measurements)
        state.gauges = gauges
        state.measurements = measurements
        state.stats = stats
      })
  },
})

function adjustMeasurements(gauges, measurements) {
  measurements.forEach(
    (measurement) =>
      (measurement.gauge = gauges.find(
        (gauge) => measurement.gaugeId === gauge.id
      ))
  )
  measurements.forEach((measurement) => {
    const previousMeasurement = Util.getPreviousMeasurement(
      measurement,
      measurements
    )
    measurement.consumption = Util.getConsumption(
      measurement,
      previousMeasurement
    )
  })
}

export const ConsumptionActions = ConsumptionSlice.actions
export default ConsumptionSlice
