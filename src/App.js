import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Layout from './views/Layout'
import Home from './views/Home'
import History from './views/History'
import Gauges from './views/Gauges'
import About from './views/About'
import Error from './views/Error'
import Login from './views/Login'

function App() {
  return (
    <BrowserRouter basename={`${process.env.REACT_APP_BASEDIR}`}>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="history" element={<History />} />
          <Route path="gauges" element={<Gauges />} />
          <Route path="about" element={<About />} />
          <Route path="login" element={<Login />} />
          <Route path="error" element={<Error />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
