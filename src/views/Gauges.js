import CtGauges from '../components/CtGauges'

const Gauges = () => {
  return (
    <div>
      <h1>Gauges</h1>
      <CtGauges />
    </div>
  )
}

export default Gauges
