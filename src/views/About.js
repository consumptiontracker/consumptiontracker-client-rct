const About = () => {
  return (
    <div>
      <h1>Consumption Tracker</h1>
      <p className="ct-text-dark">App for tracking utilities consumption.</p>
      <p className="ct-text-dark">
        Created by Marek Lints (marek.lints AT gmail.com)
      </p>
      <hr />
      <p>{new Date().getFullYear()}</p>
    </div>
  )
}

export default About
