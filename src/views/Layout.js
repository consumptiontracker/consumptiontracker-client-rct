import { Outlet, useNavigate } from 'react-router-dom'
import CtHeader from '../components/CtHeader'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchConsumptionData } from '../store/ConsumptionActions'
import Auth from '../util/Auth'
import Util from '../util/Util'
import { UserActions } from '../store/UserSlice'
import CtError from '../components/CtError'

const Layout = () => {
  const error = useSelector((state) => state.user.error)

  const dispatch = useDispatch()
  const navigate = useNavigate()

  useEffect(() => {
    if (Auth.getUsername()) {
      try {
        dispatch(UserActions.init())
        dispatch(fetchConsumptionData())
      } catch (e) {
        Util.handleRoutingOnError(e, navigate)
      }
    } else {
      navigate('/login')
    }
  }, [dispatch, navigate, error])

  return (
    <div className="layout">
      <CtHeader />
      <CtError message={error} />
      <main>
        <Outlet />
      </main>
    </div>
  )
}

export default Layout
